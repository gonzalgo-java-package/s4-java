package com.zuitt.batch193;

public interface Actions {

    //interface are used to achieve abstraction, users of our app will be able to use the methods but not be able to see how those methods work
    //Classes cannot have multiple inheritance
    //Classes can implement the multiple interface
    //you can think of interfaces as "class" for classes or blueprint for blueprint
    public void sleep();
    void run();
}
