package com.zuitt.batch193;

public class StaticPoly {

    public int addition(int a, int b){
        return a + b;
    }

    //overload by adding or changing the number of arguments
    public int addition(int a, int b, int c){
        return a + b + c;
    }

    //overload by changing the type of arguments
    public double addition(double a, double b){
        return a + b;
    }

    //method overloading: when there are multiple function with the same name but different parameters
}
