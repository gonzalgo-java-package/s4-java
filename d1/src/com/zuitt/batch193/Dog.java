package com.zuitt.batch193;

public class Dog extends Animal{

    //this is the child class that inherits the parent class(Animal)
    //extends is a keyword to inherit the variables in th Animal class

    //property
    private String breed;

    //constructor
    public Dog(){
        //super will reference the varialble from the Animal class
        super(); //Animal() Constructor by using this, we can use the instance variable, we can invoke immediate parent class constructor and class method
        this.breed = "Chihuahua";
    }

    //with parameter
    public Dog(String name, String color, String breed){
        super(name, color); //Animal(String name, String color) constructor
        this.breed = breed;
    }

    //getter
    public String getBreed(){
        return this.breed;
    }

    //method
    public void speak(){
        super.call();
        System.out.println("Bark");
    }

}
