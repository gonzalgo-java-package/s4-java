package com.zuitt.batch193;

public interface Greetings {

    void morningGreet();
    void holidayGreet();
}
