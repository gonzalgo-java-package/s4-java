package com.zuitt;

public class Main {

    public static void main(String[] args) {

        User lalisa = new User("Lalisa", "Manobal", 25, "Thailand");

        Course dance = new Course();
        dance.setName("Dancing");
        dance.setDescription("Become a dance machine");
        dance.setSeats(4);
        dance.setFee(25_000.99);
        dance.setStartDate("July 30, 2022");
        dance.setEndDate("December 25, 2022");
        dance.setInstructor(lalisa);


        System.out.println("User's first name:");
        System.out.println(lalisa.getFirstName());

        System.out.println("User's last name:");
        System.out.println(lalisa.getLastName());

        System.out.println("User's age:");
        System.out.println(lalisa.getAge());

        System.out.println("User's address:");
        System.out.println(lalisa.getAddress());

        System.out.println("Course's name:");
        System.out.println(dance.getName());

        System.out.println("Course's seats:");
        System.out.println(dance.getSeats());

        System.out.println("Course's instructors first name:");
        System.out.println(dance.getInstructor().getFirstName());


    }
}
